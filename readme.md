Word-failis on tõlkimises olevad kohad punased. Alustame eestikeelsete tekstidega.

Mängu mõte on selles, et nupust valitud riik muutub kaardil tumeroosaks ja temale lähimad kolm sugulast üksteise järel sama roosa järjest heledamadeks toonideks. 
Valitud riigi nupp muutub roosaks ning sugulaste nuppudel muutuvad roosaks raamid ja tekst. 

Valitud riik #cc0066
1. sugulane #f178a6
2. sugulane #f59dbc
3. sugulane #f9c3d5