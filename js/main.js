var lang;

var countries=[
{"name":"Btn_Ru_Tver","riigid":["#Venemaa_Tver_", "#Eesti", "#Leedu", "#Poola"],"naaberButton":["#Btn_Estonia","#Btn_Lithuania","#Btn_Poland"]},
{"name":"Btn_France","riigid":["#Prantsusmaa", "#Sveits", "#Hispaania", "#Saksamaa_Augsburg_"],"naaberButton":["#Btn_Switzerland","#Btn_Spain","#Btn_Ger_Augsburg"]},
{"name":"Btn_Poland","riigid":["#Poola", "#Leedu", "#Tsehhi", "#Ungari"],"naaberButton":["#Btn_Lithuania","#Btn_Czech","#Btn_Hungary"]},
{"name":"Btn_Lithuania","riigid":["#Leedu", "#Eesti", "#Lati", "#Poola"],"naaberButton":["#Btn_Estonia","#Btn_Latvia","#Btn_Poland"]},
{"name":"Btn_Latvia","riigid":["#Lati", "#Eesti", "#Leedu", "#Venemaa_Tver_"],"naaberButton":["#Btn_Estonia","#Btn_Lithuania","#Btn_Ru_Tver"]},
{"name":"Btn_Estonia","riigid":["#Eesti", "#Lati", "#Leedu", "#Venemaa_Tver_"],"naaberButton":["#Btn_Latvia","#Btn_Lithuania","#Btn_Ru_Tver"]},
{"name":"Btn_Spain","riigid":["#Hispaania", "#Prantsusmaa", "#Sveits", "#Saksamaa_Augsburg_"],"naaberButton":["#Btn_France","#Btn_Switzerland","#Btn_Ger_Augsburg"]},
{"name":"Btn_It_Foggia","riigid":["#Itaalia_Foggia_", "#Itaalia_Borbera_", "#Bulgaaria", "#Sveits"],"naaberButton":["#Btn_It_Borbera","#Btn_Bulgaria","#Btn_Switzerland"]},
{"name":"Btn_It_Borbera","riigid":["#Itaalia_Borbera_", "#Hispaania", "#Sveits", "#Prantsusmaa"],"naaberButton":["3Btn_Spain","#Btn_Switzerland","#Btn_France"]},
{"name":"Btn_Switzerland","riigid":["#Sveits", "#Prantsusmaa", "#Itaalia_Foggia_", "#Austria"],"naaberButton":["#Btn_France","#Btn_It_Foggia","#Btn_Austria"]},
{"name":"Btn_Bulgaria","riigid":["#Bulgaaria", "#Itaalia_Foggia_", "#Austria", "#Ungari"],"naaberButton":["#Btn_It_Foggia","#Btn_Austria","#Btn_Hungary"]},
{"name":"Btn_Ger_Augsburg","riigid":["#Saksamaa_Augsburg_", "#Saksamaa_Kiel_", "#Austria", "#Ungari"],"naaberButton":["#Btn_Ger_Kiel","#Btn_Austria","#Btn_Hungary"]},
{"name":"Btn_Ger_Kiel","riigid":["#Saksamaa_Kiel_", "#Austria", "#Tsehhi", "#Saksamaa_Augsburg_"],"naaberButton":["#Btn_Austria","#Btn_Czech","#Btn_Ger_Augsburg"]},
{"name":"Btn_Hungary","riigid":["#Ungari", "#Austria", "#Tsehhi", "#Saksamaa_Augsburg_"],"naaberButton":["#Btn_Austria","#Btn_Czech","#Btn_Ger_Augsburg"]},
{"name":"Btn_Czech","riigid":["#Tsehhi", "#Poola", "#Austria", "#Ungari"],"naaberButton":["#Btn_Poland","#Btn_Austria","#Btn_Hungary"]},
{"name":"Btn_Austria","riigid":["#Austria", "#Saksamaa_Augsburg_", "#Tsehhi", "#Ungari"],"naaberButton":["#Btn_Ger_Augsburg","#Btn_Czech","#Btn_Hungary"]},
{"name":"Btn_Fin_Helsinki","riigid":["#Soome_Helsinki_", "#Rootsi", "#Soome_Kuusamo_", "#Eesti"],"naaberButton":["#Btn_Sweden","#Btn_Fin_Kuusamo","#Btn_Estonia"]},
{"name":"Btn_Fin_Kuusamo","riigid":["#Soome_Kuusamo_", "#Soome_Helsinki_", "#Rootsi", "#Eesti"],"naaberButton":["#Btn_Fin_Helsinki","#Btn_Sweden","#Btn_Estonia"]},
{"name":"Btn_Sweden","riigid":["#Rootsi", "#Saksamaa_Kiel_", "#Saksamaa_Augsburg_", "#Austria"],"naaberButton":["#Btn_Ger_Kiel","#Btn_Ger_Augsburg","#Btn_Austria"]}
];

$( document ).ready(function() {

	$('#Ulejaanud_riigid').css({"fill":"#a0dae3"});
	$('#Venemaa_Tver_').css({ "fill": "#251f5e" });
	$('#Prantsusmaa').css({"fill":"#251f5e"});
	$('#Poola').css({"fill":"#251f5e"});
	$('#Leedu').css({"fill":"#251f5e"});
	$('#Lati').css({"fill":"#251f5e"});
	$('#Eesti').css({ "fill": "#251f5e" });
	$('#Hispaania').css({"fill":"#251f5e"});
	$('#Itaalia_Foggia_').css({"fill":"#251f5e"});
	$('#Itaalia_Borbera_').css({"fill":"#251f5e"});
	$('#Sveits').css({"fill":"#251f5e"});
	$('#Bulgaaria').css({"fill":"#251f5e"});
	$('#Saksamaa_Augsburg_').css({"fill":"#251f5e"});
	$('#Saksamaa_Kiel_').css({"fill":"#251f5e"});
	$('#Ungari').css({"fill":"#251f5e"});
	$('#Tsehhi').css({"fill":"#251f5e"});
	$('#Austria').css({"fill":"#251f5e"});
	$('#Soome_Helsinki_').css({"fill":"#251f5e"});
	$('#Soome_Kuusamo_').css({"fill":"#251f5e"});
	$('#Rootsi').css({"fill":"#251f5e"});

	// -- Get EST translations
	getTranslations('et');

	// -- Languages click function
	jQuery('.languages div').click(function(){
		lang = jQuery(this).attr('data-language');
		if(lang){
			getTranslations(lang);
		}
	});

	$(".btn_map").click(function(){
		var button = $(this).attr('id');
		console.log(this);
		$(this).children().css("fill","#cc0066");
		$("#"+button).click();
	});


	$('.btn').click(function() {
		console.log("click");
		resetMap();
		resetButtons();
		var button = $(this).attr('id');

		var country = countries.find( element => element.name === button );

		console.log(country);
		//Esimene naaber
		$(country.riigid[1]).css({ fill: "#f178a6", "transition" : "ease 1.0s"});
		//Teine naaber
		$(country.riigid[2]).css({ fill: "#f59dbc", "transition" : "ease 2.0s" });
		//Kolmas naaber
		$(country.riigid[3]).css({ fill: "#f9c3d5", "transition" : "ease 3.0s" });
		//Valitud riik
		$(country.riigid[0]).css({ fill: "#cc0066" });
		//Valitud riigi buttoni värvimine
		$(this).css({ "color":"#f1f1ec;" });
		$(this).css({ "background":"#cc0066"});
		$(this).css({ "border-color":"#cc0066" });
		//Naabite buttoni värvimine
		$(country.naaberButton[0]).css({ "color" : "#cc0066", "border-color" : "#cc0066" });
		//Teine naaber
		$(country.naaberButton[1]).css({ "color" : "#cc0066", "border-color" : "#cc0066" });
		//Kolmas naaber
		$(country.naaberButton[2]).css({ "color" : "#cc0066", "border-color" : "#cc0066" });
		});
});

function removeTransition(){
	$(country.riigid[1]).css({ fill: "#f178a6", "transition" : "1.0s"});
		//Teine naaber
	$(country.riigid[2]).css({ fill: "#f59dbc", "transition" : "2.0s" });
		//Kolmas naaber
	$(country.riigid[3]).css({ fill: "#f9c3d5", "transition" : "3.0s" });
}


function onMessage(evt){
		try{
		
		}catch(Ex){
			console.log(Ex);
		}
	}

function goToView(viewNumber){
		//clearTimer();
		if(viewNumber!=1){

		}
		$(".view").removeClass("show");
		$(".view:nth-of-type("+viewNumber+")").addClass("show");
}

function resetMap(){
	$('#Ulejaanud_riigid').css({"fill":"#a0dae3"});
	$('#Venemaa_Tver_').css({ "fill": "#251f5e", "transition" : "0s" });
	$('#Prantsusmaa').css({"fill":"#251f5e", "transition" : "0s" });
	$('#Poola').css({"fill":"#251f5e", "transition" : "0s" });
	$('#Leedu').css({"fill":"#251f5e", "transition" : "0s" });
	$('#Lati').css({"fill":"#251f5e", "transition" : "0s" });
	$('#Eesti').css({ "fill": "#251f5e", "transition" : "0s" });
	$('#Hispaania').css({"fill":"#251f5e", "transition" : "0s" });
	$('#Itaalia_Foggia_').css({"fill":"#251f5e", "transition" : "0s" });
	$('#Itaalia_Borbera_').css({"fill":"#251f5e", "transition" : "0s" });
	$('#Sveits').css({"fill":"#251f5e", "transition" : "0s" });
	$('#Bulgaaria').css({"fill":"#251f5e", "transition" : "0s" });
	$('#Saksamaa_Augsburg_').css({"fill":"#251f5e", "transition" : "0s" });
	$('#Saksamaa_Kiel_').css({"fill":"#251f5e", "transition" : "0s" });
	$('#Ungari').css({"fill":"#251f5e", "transition" : "0s" });
	$('#Tsehhi').css({"fill":"#251f5e", "transition" : "0s" });
	$('#Austria').css({"fill":"#251f5e", "transition" : "0s" });
	$('#Soome_Helsinki_').css({"fill":"#251f5e", "transition" : "0s" });
	$('#Soome_Kuusamo_').css({"fill":"#251f5e", "transition" : "0s" });
	$('#Rootsi').css({"fill":"#251f5e", "transition" : "0s" });
}


function resetButtons(){
	// $('#Btn_Fin_Kuusamo').css({ "background": "#f1f1ec", "color" : "#000", "border-color" : "#000" });
	// $('#Btn_Sweden').css({ "background": "#f1f1ec", "color" : "#000", "border-color" : "#000" });
	// $('#Btn_Fin_Helsinki').css({ "background": "#f1f1ec", "color" : "#000", "border-color" : "#000" });
	// $('#Btn_Czech').css({ "background": "#f1f1ec", "color" : "#000", "border-color" : "#000" });
	// $('#Btn_Hungary').css({ "background": "#f1f1ec", "color" : "#000", "border-color" : "#000" });
	// $('#Btn_Bulgaria').css({ "background": "#f1f1ec", "color" : "#000", "border-color" : "#000" });
	// $('#Btn_Austria').css({ "background": "#f1f1ec", "color" : "#000", "border-color" : "#000" });
	// $('#Btn_Ger_Kiel').css({ "background": "#f1f1ec", "color" : "#000", "border-color" : "#000" });
	// $('#Btn_Ger_Augsburg').css({ "background": "#f1f1ec", "color" : "#000", "border-color" : "#000" });
	// $('#Btn_France').css({ "background": "#f1f1ec", "color" : "#000", "border-color" : "#000" });
	// $('#Btn_Switzerland').css({ "background": "#f1f1ec", "color" : "#000", "border-color" : "#000" });
	// $('#Btn_It_Borbera').css({ "background": "#f1f1ec", "color" : "#000", "border-color" : "#000" });
	// $('#Btn_It_Foggia').css({ "background": "#f1f1ec", "color" : "#000", "border-color" : "#000" });
	// $('#Btn_Spain').css({ "background": "#f1f1ec", "color" : "#000", "border-color" : "#000" });
	// $('#Btn_Estonia').css({ "background": "#f1f1ec", "color" : "#000", "border-color" : "#000" });
	// $('#Btn_Latvia').css({ "background": "#f1f1ec", "color" : "#000", "border-color" : "#000" });
	// $('#Btn_Lithuania').css({ "background": "#f1f1ec", "color" : "#000", "border-color" : "#000" });
	// $('#Btn_Poland').css({ "background": "#f1f1ec", "color" : "#000", "border-color" : "#000" });
	$('.btn').css({ "background": "#f1f1ec", "color" : "#000", "border-color" : "#000" });
}

	// -- TRANSLATIONS
	function getTranslations(lang) {
		jQuery('.languages div').removeClass('active');
		jQuery('.languages div[data-language="' + lang + '"]').addClass('active');
		var json = getLangJSON(lang);
		jQuery.each(json, function (key, data) {
			jQuery.each(data, function(key1,data1){
				console.log(key1+" : "+data1);
				jQuery('[data-translation="' + key1 + '"]').html(data1).val(data1);
			});
		});
	}

	function getLangJSON(lang){
		var json;
		if(lang == 'et'){
			json = translations.et;
		} else if(lang == 'en') {
			json = translations.en;
		} else if(lang == 'ru') {
			json = translations.ru;
		} else if(lang == 'fi') {
			json = translations.fi;
		}
		return json;
	}